FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 5050

ENV ASPNETCORE_URLS=http://*:5050

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build
WORKDIR /src
COPY ["for-docker.csproj", "./"]
RUN dotnet restore "for-docker.csproj"
COPY . .
WORKDIR "/src/."
RUN dotnet build "for-docker.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "for-docker.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "for-docker.dll"]
